-- done() returns true if terminal state is reached
function done()
	if won() or lost() then return true else return false end
end

-- won() returns true if p2 lost by points, by TKO or by KO respectively
function won()
	if data.endstate == 250 or data.endstate == 252 or data.endstate == 254 then
		return true
	else
		return false
	end
end

-- lost() returns true if p1 lost by points, by TKO or by KO respectively
function lost()
	if data.endstate == 249 or data.endstate == 251 or data.endstate == 253 then
		return true
	else
		return false
	end
end

-- score() returns the current in-game score
-- function score()
-- 	score = 0
-- 	score = score + data.scoreOnes
-- 	score = score + data.scoreTens * 10
-- 	score = score + data.scoreHundreds * 100
-- 	score = score + data.scoreThousands * 1000
-- 	score = score + data.scoreTenThousands * 10000
-- 	score = score + data.scoreHundredThousands * 100000
-- 	return score
-- end

-- seconds() returns the number of seconds the fight has taken so far
function seconds()
	return data.seconds + data.tenSeconds * 10 + data.minutes * 60 + data.round * 180
end

-- reward() returns the reward for the last action taken
p1lifeold = 96
p2lifeold = 96
function reward()
	if won() then
		return seconds()
	-- elseif lost() then
	-- 	return -100
	elseif data.p1life < p1lifeold then
		p1lifeold = data.p1life
		if p1lifeold == 0 then
			return -10
		else
			return -1
		end
	elseif data.p1life > p1lifeold then
		p1lifeold = data.p1life
		return 10
	elseif data.p2life < p2lifeold then
		p2lifeold = data.p2life
		if p2lifeold == 0 then
			return 10
		else
			return 1
		end
	elseif data.p2life > p2lifeold then
		p2lifeold = data.p2life
	end	
end

